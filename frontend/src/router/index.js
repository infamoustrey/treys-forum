import Vue from 'vue';
import Router from 'vue-router';

import home from '@/views/home.vue';
import navigation from '@/views/nav.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      components: {
          nav: navigation,
          content: home
      }
    }
  ],
  mode: 'history'
})
