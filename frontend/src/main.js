
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import bulma from 'bulma'

Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
});
