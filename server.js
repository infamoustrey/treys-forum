const express = require('express');
const app = express();

app.use(require('helmet')());
app.use(express.static('frontend/dist'));

app.use(require('./routes'));

const Topic = require('./model/Topic');

new Topic({description: 'General Discussion'}).save().then((topic) => {
    console.log(topic);
});

app.listen(3000, () => console.log('App Listening on port 3000'));