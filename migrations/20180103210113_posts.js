
exports.up = function(knex, Promise) {

    return knex.schema.createTable('posts', table => {

        table.increments();
        table.integer('thread_id');
        table.integer('user_id');
        table.text('content');
        table.timestamps();

    });

};

exports.down = function(knex, Promise) {

    return knex.schema.dropTableIfExists('posts');

};
