
exports.up = function(knex, Promise) {

    return knex.schema.createTable('conversation_members', table => {

        table.increments();
        table.integer('conversation_id');
        table.integer('user_id');
        table.timestamps();

    });

};

exports.down = function(knex, Promise) {

    return knex.schema.dropTableIfExists('conversation_members');

};
