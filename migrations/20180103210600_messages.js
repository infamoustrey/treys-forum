
exports.up = function(knex, Promise) {

    return knex.schema.createTable('messages', table => {

        table.increments();
        table.integer('conversation_id');
        table.integer('user_id');
        table.text('message');
        table.timestamps();

    });

};

exports.down = function(knex, Promise) {

    return knex.schema.dropTableIfExists('messages');

};
