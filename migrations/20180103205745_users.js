
exports.up = function(knex, Promise) {

    return knex.schema.createTable('users', table => {
        table.increments();
        table.string('name');
        table.string('username');
        table.timestamps();
    });

};

exports.down = function(knex, Promise) {

    return knex.schema.dropTableIfExists('users');

};
