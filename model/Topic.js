
let bookshelf = require('../bookshelf');
let Threads = require('./Thread');

let Topic = bookshelf.Model.extend({
    tableName: 'topics',
    threads(){
        return this.hasMany(Threads);
    }
});

module.exports = Topic;