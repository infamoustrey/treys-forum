
const bookshelf = require('../bookshelf');
const Topic = require('./Topic');

const Thread = bookshelf.Model.extended({
    tableName: 'threads',
    threads(){
        return this.belongsTo(Topic);
    }
});

module.exports = Thread;